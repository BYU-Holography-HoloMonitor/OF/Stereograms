#pragma once

#include <string>

#include "ofMain.h"
#include "ofxDatGui.h"
#include "ofxJSON.h"
#include "ScreenGrab.h"

#define _PI 3.141592653589793238462643383279


#define N_MAX_SLICES 10 // this number must be equal to that in shader.frag
												// this is just to have arrays of fixed size so that CPU-GPU communication is simpler
												// feel free to increase this amount if necessary



/* TODO: (21 Jun 2016 - DH)




item->onInternalEvent(this, &ofxDatGui::onInternalEventCallback);
layoutGui();

ofxDatGuiDropdown(string label, const vector<string>& options = vector<string>()) : ofxDatGuiGroup(label)






MODE changes:

mode flag (simpler version of GUI dropdown menu)
file loading
video updates
screen capture updates

FBO updates in update()

check speed penalty of converting 2x2 to 4x1 for quad view
 - tested in Window Viewing (Mario Kart N64)
 - was 60 FPS without and with conversion


verify limiting/changing of output_image_width widget from sources_fbo.getWidth()

*/


/*
things to do now: (24, 27 Jun 2016)

actually test it to see if it works - stereograms work!

autoupdate from file every X seconds - done

video mode

quad-view mode

screen grab mode

white-balance angle function?




there will have to be default source files (images, video, JSON config files...)
so that when you switch modes, there's something usable to demonstrate functionality



*/




/* MODIFICATIONS TO ofDatGui library:

ability to change drop-down menu items - ofxDatGuiDropdown::ofxDatGuiDropdown(...), line 436

*/
//in ofxDatGuiGroups.h:
//...
//class ofxDatGuiDropdown : public ofxDatGuiGroup
//{
//
//public:
//
//  ofxDatGuiDropdown( string label, const vector<string>& options = vector<string>() ) : ofxDatGuiGroup( label )
//  {
//    mOption = 0;
//    mType = ofxDatGuiType::DROPDOWN;
//
//    // code change 21 Jul 2016 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//    setNewOptions( options );
//    /*
//    for(int i=0; i<options.size(); i++){
//    ofxDatGuiDropdownOption* opt = new ofxDatGuiDropdownOption(options[i]);
//    opt->setIndex(children.size());
//    opt->onButtonEvent(this, &ofxDatGuiDropdown::onOptionSelected);
//    children.push_back(opt);
//    }
//    */
//
//    setTheme( ofxDatGuiComponent::theme.get() );
//  }
//
//  // added code 21 Jul 2016  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//  void setNewOptions( const vector<string>& options = vector<string>() )
//  {
//    // delete old options
//    for( int i=0; i<children.size(); i++ )
//      delete children[ i ];
//
//    children.clear();
//
//    // add new options
//    for( int i=0; i<options.size(); i++ )
//    {
//      ofxDatGuiDropdownOption* opt = new ofxDatGuiDropdownOption( options[ i ] );
//      opt->setIndex( children.size() );
//      opt->onButtonEvent( this, &ofxDatGuiDropdown::onOptionSelected );
//      children.push_back( opt );
//    }
//
//    //setTheme( ofxDatGui::theme.get() ); // not working as expected
//  }
//  // end of added code
//
//  void setTheme( ofxDatGuiTheme* theme )
//...
/*

change of input text to lowercase in ofxDatGuiTheme.h, line 188
see also https://forum.openframeworks.cc/t/ofxdatgui-a-new-user-interface-for-of/20553/55?u=dhenrie0208

//in ofxDatGuiTheme.h:
            struct {
                int highlightPadding = 5;
                bool forceUpperCase = false; // true; // https://forum.openframeworks.cc/t/ofxdatgui-a-new-user-interface-for-of/20553/55?u=dhenrie0208
            } textInput;
*/



/* MODIFICATIONS TO openframeworks library:

fixed BGR / RGB swapping
  in void ofPixels_<PixelType>::swapRgb()
    in \libs\openFrameworks\graphics\ofPixels.cpp
see dhenrie0208's posts at https://forum.openframeworks.cc/t/recursive-screen-recording-solution-for-desktop-feedback-loop/22491

*/


struct StereogramSlice
{
	string source;
	//short int source_width; // TODO: evaluate if necessary

	bool enabled : 1;

	bool blanking : 1; // 0 - normal operation
										 // 1 - blank white
	short int y_offset;

	float angle; // output angle

	float freq_r; // frequency for red   component; correlated with angle
	float freq_g; // frequency for green component; correlated with angle
	float freq_b; // frequency for blue  component; correlated with angle

	float ampl_r; // amplitude weight for red   component; correlated with angle
	float ampl_g; // amplitude weight for green component; correlated with angle
	float ampl_b; // amplitude weight for blue  component; correlated with angle
};

class AppGuiTheme : public ofxDatGuiTheme
{
public:
	AppGuiTheme()
	{
		font.size = 10;
		layout.width = 300;
		layout.textInput.forceUpperCase = false; // this doesn't work; force it by changing initial value in ofxDatGuiTheme.h
		layout.upperCaseLabels = false;          // this doesn't work; force it by changing initial value in ofxDatGuiTheme.h
		init();
	};
};

class outputApp; // forward declaration; see http://stackoverflow.com/questions/28697567/c-class-and-interlinked-objects-forming-a-cycle

class GUIApp : public ofBaseApp
{
public:

	unsigned int PIXELS_H_ACTIVE;
	unsigned int PIXELS_V_ACTIVE;
	unsigned int COMP_ROWS_PER_HOLOLINE;
	unsigned int N_HOLOLINES;
	float pixelClockMHz;
	float inv_pixelClockMHz_2_PI; // convenience variable; see GUIApp::setup() and outputApp::draw()
	// unsigned int ACTIVE_PIXELS_PER_HOLOLINE ( PIXELS_H_ACTIVE * COMP_ROWS_PER_HOLOLINE )


	unsigned int output_window_count; // set in main.cpp
	shared_ptr<outputApp> output_app0;
	shared_ptr<ofAppBaseWindow> output_window0;
	bool output_window0_isFullScreen;

	shared_ptr<ofAppBaseWindow> gui_window;

	void GUIApp::setOutputWindowFullscreen( shared_ptr<ofAppBaseWindow> window, bool fullScreen );
	int set_output_window_fullscreen_oneshot;


	ofxDatGui* gui0;
	ofxDatGuiTheme* gu0_theme;


	// general control widgets
	ofxDatGuiTextInput* gui0_general_Title; // save to config file
	ofxDatGuiButton*    gui0_general_Load;
	ofxDatGuiButton*    gui0_general_Save;
	ofxDatGuiLabel*     gui0_general_TopLabel;
	ofxDatGuiSlider*    gui0_general_OutputGain; // save to config file
	ofxDatGuiToggle*    gui0_general_FlipHorizontal; // save to config file
	ofxDatGuiToggle*    gui0_general_FlipVertical; // save to config file
	ofxDatGuiToggle*    gui0_general_RollVertical; // save to config file
	ofxDatGuiTextInput* gui0_general_OutputImageWidth; // save to config file
	ofxDatGuiDropdown*  gui0_general_StereogramMode; // save to config file
	vector<string> mode_options;
	enum StereogramMode { POLY_STATIC, MONO_VIDEO, QUAD_VIEW, MONO_SCREEN } stereogram_mode;

	// widgets that mainly pertain to the POLY_STATIC mode
	ofxDatGuiTextInput* gui0_general_SliceCount; // don't save since we'll know # of slices anyways
	unsigned int n_slices;
	ofxDatGuiTextInput* gui0_general_ReloadPeriod;
	//int reload_period; // measured in integer seconds; 0 means don't reload
	float last_reload_time;

	// slice-specific control widgets
	ofxDatGuiDropdown*  gui0_slice_SelectedSlice; // don't save
	ofxDatGuiLabel*     gui0_slice_TopLabel;
	ofxDatGuiTextInput* gui0_slice_Source;
	ofxDatGuiToggle*    gui0_slice_Enabled;
	ofxDatGuiToggle*    gui0_slice_Blank;
	ofxDatGuiTextInput* gui0_slice_YOffset;
	ofxDatGui2dPad*     gui0_slice_YOffset2D;
	ofxDatGuiSlider*    gui0_slice_Angle;
	ofxDatGuiSlider*    gui0_slice_RedFreq;
	ofxDatGuiSlider*    gui0_slice_RedAmpl;
	ofxDatGuiSlider*    gui0_slice_GreenFreq;
	ofxDatGuiSlider*    gui0_slice_GreenAmpl;
	ofxDatGuiSlider*    gui0_slice_BlueFreq;
	ofxDatGuiSlider*    gui0_slice_BlueAmpl;

	void updateWidgetsFromSlice( unsigned int slice_i );


	// ofxDatGui events are defined in GUIApp_ofxDatGUIEvents.cpp
	void onButtonEvent( ofxDatGuiButtonEvent e );
	void onDropDownEvent( ofxDatGuiDropdownEvent e );
	void onTextInputEvent( ofxDatGuiTextInputEvent e );
	void onSliderEvent( ofxDatGuiSliderEvent e );
	void onToggleEvent( ofxDatGuiToggleEvent e );
	void on2DPadEvent( ofxDatGui2dPadEvent e );


	ofxDatGuiComponent* last_widget;



	StereogramSlice slices[N_MAX_SLICES];

	ofFbo sources_fbo;
	// sources_fbo is a combined buffer of all of the slices;
	// each slice is vertically stacked, so if you want to access
	//  coordinate (x, y) of slice i, then access
	//  coordinate (x, y + i * N_HOLOLINES) of sources_fbo
	// each slice must be N_HOLOLINES tall; if it's not, scale it down
	// Frame Buffer Object documentation: http://openframeworks.cc/documentation/gl/ofFbo/

	ofVideoPlayer video_player;
	// only one video player for now?

  ScreenGrab screen_grab;
  // only one screen grabber for now?
  bool show_screen_grab_in_gui;


	void clearSlice( int slice_i );

	int focused_slice;
	int focused_slice_last;

	void shiftHoloImage( unsigned int slice_i, char dir );

	void drawSliceConfig( unsigned int slice_i, int x, int y );

	void redrawAllSlices();

	ofTrueTypeFont font;
	ofTrueTypeFont font_big;

	unsigned char redraw_slices; // give the ability to redraw multiple times
	// for some reason, redrawing the first time draw() is called doesn't work
	// this variable should be considered more like a boolean and can be used like a boolean
	// ie, you can do
	//   redraw_slices = true;
	// and it sets redraw_slices to 1 and redraws once
	// see the draw() function and onDropdownEvents to see where this is mainly used

	int debug_int;
	int debug_x;
	int debug_y;


	// load / save functions are defined in GUIApp_SaveLoad.cpp
	void loadStereogramFromFile( string file_path );
	void loadSlicesFromFiles();
	void saveStereogramToFile( string file_path );

	string json_config_file_directory;

	//unsigned char wait_count_before_loading_startup_JSON_file;
	// string startup_JSON_file;



	void setup();
	void update();
	void draw();

	void keyPressed( int key );
	void keyReleased( int key );

	bool shift_modifier;
	bool alt_modifier;
	bool ctrl_modifier;

	void windowResized( int w, int h );

	//void mouseMoved( int x, int y );
	void mouseDragged( int x, int y, int button );
	void mousePressed( int x, int y, int button );
	//void mouseReleased( int x, int y, int button );
	//void mouseEntered( int x, int y );
	//void mouseExited( int x, int y );
	//void dragEvent( ofDragInfo dragInfo );
	//void gotMessage( ofMessage msg );
  void exit();
};
