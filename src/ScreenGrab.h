#pragma once

#include "ofImage.h"

using namespace std;

/* ACKNOWLEDGEMENT OF EXTERNAL WORK:

the core functionality of this code is heavily based off of the contributions of OpenFrameworks Forum users "msf567" and "lilive"
see https://forum.openframeworks.cc/t/recursive-screen-recording-solution-for-desktop-feedback-loop/22491/18

*/

/* MODIFICATIONS TO openframeworks library:

fixed BGR / RGB swapping
  in void ofPixels_<PixelType>::swapRgb()
    in \libs\openFrameworks\graphics\ofPixels.cpp

see dhenrie0208's posts at https://forum.openframeworks.cc/t/recursive-screen-recording-solution-for-desktop-feedback-loop/22491

this is used ofImage::setFromPixels(...)

*/

class ScreenGrab
{
public:
  
  void initScreenGrab( string window_title,
                       int capt_src_x, int capt_src_y,
                       int capt_src_w, int capt_src_h, 
                       int capt_dst_w, int capt_dst_h );
  void updateScreenGrab();

  ofImage capture_img;

  bool isReady();

  int getCaptSrcX();
  int getCaptSrcY();
  int getCaptSrcW();
  int getCaptSrcH();
  int getCaptDstW();
  int getCaptDstH();

private:

  string window_title;

  int capt_src_w;
  int capt_src_h;
  int capt_src_x;
  int capt_src_y;
  int capt_dst_w;
  int capt_dst_h;

  HDC hSourceDC;
  HDC hCaptureDC;
  HBITMAP hCaptureBitmap;
  HWND window_handle;
  BITMAPINFO bmi = { 0 };

  unsigned char* capturedPixelData;
};