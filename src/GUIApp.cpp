#include "GUIApp.h"
#include "outputApp.h"




//--------------------------------------------------------------
void GUIApp::setup()
{
  // initialize important / widget-impacting varibles
  PIXELS_H_ACTIVE = 3872;
  PIXELS_V_ACTIVE = 3293;
  COMP_ROWS_PER_HOLOLINE = 64;
  N_HOLOLINES = 52;
  pixelClockMHz = 400.0;
  inv_pixelClockMHz_2_PI = 2.0 * _PI / pixelClockMHz;

  n_slices = 7; // dummy value for starts
  focused_slice_last = focused_slice = 0;

  sources_fbo.allocate( 1, n_slices * N_HOLOLINES, GL_RGB );



  // setup dummy initial slice configuration

  for( int i=0; i < N_MAX_SLICES; i++ )
    clearSlice( i );

  slices[ 0 ].enabled = true;
  slices[ 0 ].blanking = true;
  slices[ 0 ].y_offset = 550;
  slices[ 0 ].angle = -1.0;
  slices[ 0 ].freq_r = 51.0;
  slices[ 0 ].freq_g = 101.0;
  slices[ 0 ].freq_b = 151.0;
  slices[ 0 ].ampl_r = 0.70;
  slices[ 0 ].ampl_g = 0.80;
  slices[ 0 ].ampl_b = 0.90;
  slices[ 0 ].source = "dummy_demo.txt";



  // setup gui
  ofxDatGui::setAssetPath( "/" );
  // change ofxDatGui asset path from 
  //   "../../../../../addons/ofxDatGui/" ("of_v0.9.3_vs_release/addons/ofxDatGui/")
  // to
  //   "Sweeper_Suite/bin/data/"
  gui0 = new ofxDatGui( ofxDatGuiAnchor::TOP_RIGHT );
  gu0_theme = new AppGuiTheme();
  gui0->setTheme( gu0_theme );



  // general control widgets, with arbitrary initial values

  gui0_general_Title          = gui0->addTextInput( "Title", "Untitled" );
  gui0_general_Load           = gui0->addButton( "[Load from file]" );
  gui0_general_Save           = gui0->addButton( "[Save to file]" );

  gui0_general_TopLabel = gui0->addLabel( "General Controls" );
  gui0_general_TopLabel->setLabelAlignment( ofxDatGuiAlignment::CENTER );

  gui0_general_OutputGain     = gui0->addSlider( "Output Gain", 1.0, 10.0, 1.0 );
  gui0_general_FlipHorizontal = gui0->addToggle( "Flip Horizontal", false );
  gui0_general_FlipVertical   = gui0->addToggle( "Flip Vertical", false );
  gui0_general_RollVertical   = gui0->addToggle( "Roll Vertical", false );

  char temp_char_array[ 32 ];
  sprintf( temp_char_array, "%u", 600 );
  gui0_general_OutputImageWidth = gui0->addTextInput( "Output Width", temp_char_array );
  gui0_general_OutputImageWidth->setInputType( ofxDatGuiInputType::NUMERIC );


  mode_options = { "Input: Static Images", "Input: Single Video", "Input: Quad View", "Input: Screen Capture" };
  gui0_general_StereogramMode = gui0->addDropdown( "", mode_options );
  stereogram_mode = GUIApp::POLY_STATIC;
  gui0_general_StereogramMode->select( stereogram_mode );

  sprintf( temp_char_array, "%u", n_slices );
  gui0_general_SliceCount     = gui0->addTextInput( "Slice Count", temp_char_array );
  gui0_general_SliceCount->setInputType( ofxDatGuiInputType::NUMERIC );

  //reload_period = 0;
  last_reload_time = 0.0;
  //sprintf( temp_char_array, "%u", reload_period );
  gui0_general_ReloadPeriod   = gui0->addTextInput( "Reload Sec", "0" );
  gui0_general_ReloadPeriod->setInputType( ofxDatGuiInputType::NUMERIC );



  // slice-specific control widgets, with arbitrary initial values

  gui0_slice_TopLabel = gui0->addLabel( "Slice-Specific Controls" );
  gui0_slice_TopLabel->setLabelAlignment( ofxDatGuiAlignment::CENTER );

  vector<string> selected_slice_options;
  for( int i=0; i < n_slices; i++ )
  {
    sprintf( temp_char_array, "Slice %u", i );
    selected_slice_options.push_back( temp_char_array );
  }
  gui0_slice_SelectedSlice  = gui0->addDropdown( "", selected_slice_options );
  gui0_slice_SelectedSlice->select( focused_slice );

  gui0_slice_Source    = gui0->addTextInput( "Source", "---" );

  gui0_slice_Enabled   = gui0->addToggle( "Enabled", false );
  gui0_slice_Blank     = gui0->addToggle( "Blank", false );

  gui0_slice_YOffset   = gui0->addTextInput( "Y-Offset", "0" );
  gui0_slice_YOffset->setInputType( ofxDatGuiInputType::NUMERIC );

  gui0_slice_YOffset2D = gui0->add2dPad( "Y-Offset", ofRectangle( 0, 0, COMP_ROWS_PER_HOLOLINE, N_HOLOLINES ) );
  gui0_slice_YOffset2D->setPoint( ofPoint( 0, 0, 0 ) );

  gui0_slice_Angle     = gui0->addSlider( "Angle", -10.0, 10.0, 0.0 );
  gui0_slice_RedFreq   = gui0->addSlider( "Red Freq", 0.0, 200.0, 50.0 );
  gui0_slice_RedAmpl   = gui0->addSlider( "Red Ampl", 0.0, 1.0, 1.0 );
  gui0_slice_GreenFreq = gui0->addSlider( "Green Freq", 0.0, 200.0, 100.0 );
  gui0_slice_GreenAmpl = gui0->addSlider( "Green Ampl", 0.0, 1.0, 1.0 );
  gui0_slice_BlueFreq  = gui0->addSlider( "Blue Freq", 0.0, 200.0, 150.0 );
  gui0_slice_BlueAmpl  = gui0->addSlider( "Blue Ampl", 0.0, 1.0, 1.0 );



  // set slider precision
  gui0_slice_Angle->setPrecision( 2, false );
  gui0_slice_RedFreq->setPrecision( 2, false );
  gui0_slice_RedAmpl->setPrecision( 2, false );
  gui0_slice_GreenFreq->setPrecision( 2, false );
  gui0_slice_GreenAmpl->setPrecision( 2, false );
  gui0_slice_BlueFreq->setPrecision( 2, false );
  gui0_slice_BlueAmpl->setPrecision( 2, false );



  // update all slice-specific widgets based upon
  updateWidgetsFromSlice( focused_slice );



  // add event handlers
  gui0->onButtonEvent( this, &GUIApp::onButtonEvent );
  gui0->onDropdownEvent( this, &GUIApp::onDropDownEvent );
  gui0->onTextInputEvent( this, &GUIApp::onTextInputEvent );
  gui0->onSliderEvent( this, &GUIApp::onSliderEvent );
  gui0->onToggleEvent( this, &GUIApp::onToggleEvent );
  gui0->on2dPadEvent( this, &GUIApp::on2DPadEvent );



  // initialize modifier keys
  alt_modifier   = false;
  shift_modifier = false;
  ctrl_modifier  = false;



  // initialize other variables
  last_widget = gui0_slice_YOffset2D;
  debug_int = 0;
  debug_x = 0;
  debug_y = 0;
  ofBackground( 8, 16, 32, 255 );
  ofSetColor( 255 );
  ofSetWindowTitle( "BYU ElectroHolography Stereograms" );



  // fullscreen the output window
  output_window0_isFullScreen = false;
  // this is stupid; there should be a better way to tell if a window is fullscreened or not.
  // assume it starts windowed

  // wait until the output window starts drawing before fullscreening it
  // this is implemented in the draw() of outputApp
  // if we ever get to >=3 windows, this code is going to have to change
  set_output_window_fullscreen_oneshot = false;



  // load fonts

  //ofTrueTypeFont::setGlobalDpi( 72 ); // this screws up the font size

  font.load( "FAFERS_Technical_Font.ttf", 14, false );
  if( !font.isLoaded() )
    font.load( "ofxbraitsch/fonts/Verdana.ttf", 12, false );
  if( !font.isLoaded() )
    font.load( "arial.ttf", 14, false );
  font.setLineHeight( font.getSize() * 1.125 );
  font.setLetterSpacing( 1.037 );

  // use a slightly larger font for the "image didn't load" slice image
  font_big.load( "FAFERS_Technical_Font.ttf", 18, false );
  if( !font_big.isLoaded() )
    font_big.load( "ofxbraitsch/fonts/Verdana.ttf", 13, false );
  if( !font_big.isLoaded() )
    font_big.load( "arial.ttf", 18, false );
  font_big.setLineHeight( font_big.getSize() * 1.125 );
  font_big.setLetterSpacing( 1.037 );

  //for( int i=0; i < 16; i++ )
  //  for( int j=0; j < 16; j++ )
  //  {
  //    char c_temp_array[2];
  //    c_temp_array[0] = (char) ( j + i * 16 );
  //    c_temp_array[1] = '\0';
  //    font.drawString( (string) c_temp_array, j * font.getSize(), i * font.getSize() + 200.0 );
  //  }




  // disable auto-clearing to take up less CPU
  // ie, clear and redraw local regions that have changed so that
  // you don't have to redraw everything (esp. a lot of stuff that
  // hasn't changed) again every frame
  ofSetBackgroundAuto( false );

  // when we redraw everything every frame, we get 20fps drawing the text for >3 slices
  // (oddly enough, we get 30fps for drawing <=2 slices)
  // when we redraw things only when things change, we get 60fps

  redraw_slices = 4;
  // redraw everything four times only at the beginning since the first three times it's drawn, nothing appears
  // I don't know why draw() doesnt draw stuff the first three times it's called; it's weird - DH 10 Jun 2016

  // if things don't appear at the beginning, try increasing this number



  // load from a configuration file now that all the widgets are instantiated

  //wait_count_before_loading_startup_JSON_file = 5;
  //startup_JSON_file = "C:\\Users\\ecestudent\\Desktop\\webninja\\holo2\\public\\images\\andy_stereogram.sto";
  //// this is really stupid, but in order to load the images referenced by the JSON file,
  loadStereogramFromFile( "data\\demo files\\multi static demo.sto" );

  // draw screen grab for testing
  show_screen_grab_in_gui = true;
  screen_grab.initScreenGrab( "Untitled - Notepad",
                              0, 0, 140, 52 * 2, // 320, 240,
                              140, 52 * 2 ); // 320, 240 ); // 
}



//--------------------------------------------------------------
void GUIApp::clearSlice( int slice_i )
{
  slices[ slice_i ].source = "uninitialized";

  slices[ slice_i ].enabled = false;

  slices[ slice_i ].blanking = false;

  slices[ slice_i ].y_offset = 0;

  slices[ slice_i ].angle = 0.0;

  slices[ slice_i ].freq_r = 50.0;
  slices[ slice_i ].freq_g = 100.0;
  slices[ slice_i ].freq_b = 150.0;

  slices[ slice_i ].ampl_r = 1.0;
  slices[ slice_i ].ampl_g = 1.0;
  slices[ slice_i ].ampl_b = 1.0;
}



//--------------------------------------------------------------
void GUIApp::redrawAllSlices()
{
  ofClear( ofGetBackgroundColor() ); // TODO: verify this isn't redundant

  sources_fbo.draw( 0.0, 0.0 );

  // do initial drawings of slice configuration printouts
  for( int i=0; i < n_slices; i++ )
  {
    drawSliceConfig( i, sources_fbo.getWidth() + font.getLineHeight(), N_HOLOLINES * i );
  }
}



//--------------------------------------------------------------
void GUIApp::updateWidgetsFromSlice( unsigned int slice_i )
{
  printf( "Focused on slice %i\n", focused_slice );

  gui0_slice_SelectedSlice->select( focused_slice );

  gui0_slice_Enabled->setChecked( slices[ focused_slice ].enabled );

  char temp_char_array[ 16 ];
  sprintf( temp_char_array, "%i", slices[ focused_slice ].y_offset );
  gui0_slice_YOffset->setText( temp_char_array );

  gui0_slice_YOffset2D->setPoint( ofPoint( slices[ focused_slice ].y_offset % COMP_ROWS_PER_HOLOLINE, slices[ focused_slice ].y_offset / COMP_ROWS_PER_HOLOLINE ) );

  gui0_slice_Blank->setChecked( slices[ focused_slice ].blanking );
  gui0_slice_Angle->setValue( slices[ focused_slice ].angle );
  gui0_slice_RedFreq->setValue( slices[ focused_slice ].freq_r );
  gui0_slice_RedAmpl->setValue( slices[ focused_slice ].ampl_r );
  gui0_slice_GreenFreq->setValue( slices[ focused_slice ].freq_g );
  gui0_slice_GreenAmpl->setValue( slices[ focused_slice ].ampl_g );
  gui0_slice_BlueFreq->setValue( slices[ focused_slice ].freq_b );
  gui0_slice_BlueAmpl->setValue( slices[ focused_slice ].ampl_b );
  gui0_slice_Source->setText( slices[ focused_slice ].source );
}



//--------------------------------------------------------------
void GUIApp::update()
{

  switch( stereogram_mode )
  {
  case GUIApp::POLY_STATIC:
  {
    // check to see if we need to periodically reload the images
    // ie, if we're taking pictures and overwriting the old files
    // ideally, we would have signals from the OS indicating the time stamps
    // on these files were changing, but it's way easier to just check after
    // a couple of seconds

    int temp_reload_period = atoi( gui0_general_ReloadPeriod->getText().c_str() );

    if( temp_reload_period != 0.0 )
    {
      float temp_time = ofGetElapsedTimef();

      if( int( temp_time - last_reload_time ) >= temp_reload_period )
      {
        last_reload_time = temp_time;

        loadSlicesFromFiles(); // this also clears + redraws everything
      }
    }
  }
  break;

  case GUIApp::MONO_VIDEO:

    video_player.update();

    sources_fbo.begin();
    video_player.draw( 0.0, 0.0 );
    sources_fbo.end();

    break;

  case GUIApp::QUAD_VIEW:
  {
    int dst_w = screen_grab.getCaptDstW();
    int dst_h = screen_grab.getCaptDstH();

    sources_fbo.begin();
    //                                     draw x draw y      width        height       src x        src y
    screen_grab.capture_img.drawSubsection( 0.0, 0.0, dst_w * 0.5, dst_h * 0.5, 0.0, 0.0 );
    screen_grab.capture_img.drawSubsection( 0.0, dst_h * 0.5, dst_w * 0.5, dst_h * 0.5, dst_w * 0.5, 0.0 );
    screen_grab.capture_img.drawSubsection( 0.0, dst_h * 1.0, dst_w * 0.5, dst_h * 0.5, 0.0, dst_h * 0.5 );
    screen_grab.capture_img.drawSubsection( 0.0, dst_h * 1.5, dst_w * 0.5, dst_h * 0.5, dst_w * 0.5, dst_h * 0.5 );

    sources_fbo.end();
  }
  break;

  case GUIApp::MONO_SCREEN:
    // crop or stretch?
    break;
  }
}



//--------------------------------------------------------------
void GUIApp::drawSliceConfig( unsigned int slice_i, int x, int y )
{
  ofColor previous_color = ofGetCurrentRenderer()->getStyle().color;

  ofColor default_text_color;

  if( slice_i == focused_slice || focused_slice == -1 )
    default_text_color = ofColor( 192, 255, 64 );
  else
    default_text_color = ofColor( 192, 192, 192 );

  ofColor color_red = ofColor( 192, 64, 64 );
  ofColor color_green = ofColor( 0, 170, 0 );
  ofColor color_blue = ofColor( 96, 96, 255 );

  float font_line_height = font.getLineHeight();

  StereogramSlice* s = &slices[ slice_i ];

  // rough outline: 
  //
  // Slice 0  [x, y]   [freq_r] MHz [ampl_r]%
  // ena/bla  [angle]� [freq_g] MHz [ampl_g]%
  // source_filename   [freq_b] MHz [ampl_b]%



  // make our lives easier by erasing the entire config data printout (of this slice) as opposed to one specific element
  ofSetColor( ofColor( 64, 64, 64 ) );
  ofRect( x, y, 16 * font_line_height, 3.25*font_line_height );



  // draw 1st column

  char temp_c_array[ 256 ];
  sprintf( temp_c_array, "Slice %u\n\n%s", slice_i, slices[ slice_i ].source.c_str() );
  ofSetColor( default_text_color );
  font.drawString( temp_c_array, x + 0 * 4 * font_line_height, y + ( 0 + 1 ) * font_line_height );


  if( s->enabled )
  {
    if( s->blanking )
    {
      strcpy( temp_c_array, "blanking" );
      ofSetColor( 255, 255, 255 );
    }
    else
    {
      strcpy( temp_c_array, "enabled" );
    }
  }
  else
  {
    strcpy( temp_c_array, "disabled" );
    ofSetColor( 254, 132, 81 );
  }

  font.drawString( temp_c_array, x + 0.0, y + ( 1 + 1 ) * font_line_height );

  ofSetColor( default_text_color );



  // draw 2nd column

  sprintf( temp_c_array, "%i, %i\n%.2f*", // can't get degree symbol working; font.drawString(...) might not be able to print characters > 127
           slices[ slice_i ].y_offset % COMP_ROWS_PER_HOLOLINE,
           slices[ slice_i ].y_offset / COMP_ROWS_PER_HOLOLINE,
           slices[ slice_i ].angle );
  font.drawString( temp_c_array, x + 1 * 4 * font_line_height, y + ( 0 + 1 ) * font_line_height );



  // draw red/green/blue channel info

  //sprintf( temp_c_array, "%.2f MHz  %.0f%%", slices[slice_i].freq_r, slices[slice_i].ampl_r * 100.0 );
  //ofSetColor( color_red );
  //font.drawString( temp_c_array, x + 2 * 4 * font_line_height, y + ( 0 + 1 ) * font_line_height );

  //sprintf( temp_c_array, "%.2f MHz  %.0f%%", slices[slice_i].freq_g, slices[slice_i].ampl_g * 100.0 );
  //ofSetColor( color_green );
  //font.drawString( temp_c_array, x + 2 * 4 * font_line_height, y + ( 1 + 1 ) * font_line_height );

  //sprintf( temp_c_array, "%.2f MHz  %.0f%%", slices[slice_i].freq_b, slices[slice_i].ampl_b * 100.0 );
  //ofSetColor( color_blue );
  //font.drawString( temp_c_array, x + 2 * 4 * font_line_height, y + ( 2 + 1 ) * font_line_height );



  // draw 3rd column; tedious because of the different text colors

  sprintf( temp_c_array, "%.2f MHz", slices[ slice_i ].freq_r );
  ofSetColor( color_red );
  font.drawString( temp_c_array, x + 2 * 4 * font_line_height, y + ( 0 + 1 ) * font_line_height );

  sprintf( temp_c_array, "%.2f MHz", slices[ slice_i ].freq_g );
  ofSetColor( color_green );
  font.drawString( temp_c_array, x + 2 * 4 * font_line_height, y + ( 1 + 1 ) * font_line_height );

  sprintf( temp_c_array, "%.2f MHz", slices[ slice_i ].freq_b );
  ofSetColor( color_blue );
  font.drawString( temp_c_array, x + 2 * 4 * font_line_height, y + ( 2 + 1 ) * font_line_height );



  // draw 4th column; tedious because of the different text colors

  sprintf( temp_c_array, "%.0f%%", slices[ slice_i ].ampl_r * 100.0 );
  ofSetColor( color_red );
  font.drawString( temp_c_array, x + 3.5 * 4 * font_line_height, y + ( 0 + 1 ) * font_line_height );

  sprintf( temp_c_array, "%.0f%%", slices[ slice_i ].ampl_g * 100.0 );
  ofSetColor( color_green );
  font.drawString( temp_c_array, x + 3.5 * 4 * font_line_height, y + ( 1 + 1 ) * font_line_height );

  sprintf( temp_c_array, "%.0f%%", slices[ slice_i ].ampl_b * 100.0 );
  ofSetColor( color_blue );
  font.drawString( temp_c_array, x + 3.5 * 4 * font_line_height, y + ( 2 + 1 ) * font_line_height );



  // restore previous draw color before drawSliceConfig was called
  ofSetColor( previous_color );
}



//--------------------------------------------------------------
void GUIApp::draw() // extra drawing tasks for the GUI
{
  if( redraw_slices )
  {
    ofClear( ofGetBackgroundColor() );

    ofSetColor( 255 );

    sources_fbo.draw( 0.0, 0.0 );

    redrawAllSlices();

    redraw_slices--;
  }



  switch( stereogram_mode )
  {
  case GUIApp::POLY_STATIC:
    // do nothing; it will be redrawn as needed
    break;

  case GUIApp::MONO_VIDEO:
  case GUIApp::QUAD_VIEW:
  case GUIApp::MONO_SCREEN:

    // redraw the FBO for video (file / screen capture) updates

    sources_fbo.draw( 0.0, 0.0 );

    break;
  }



  // draw screen capture for debugging if needed

  if( show_screen_grab_in_gui )
  {
    screen_grab.updateScreenGrab();
    screen_grab.capture_img.draw( 0.0, ofGetHeight() - screen_grab.capture_img.getHeight() );
  }



  // draw GUI FPS

  float font_line_height = font.getLineHeight();

  ofSetColor( 64, 64, 64 );
  ofRect( ofGetWidth() - 9 * font_line_height, ofGetHeight() - 2 * font_line_height, 9 * font_line_height, 2 * font_line_height );

  char temp_c_array[ 32 ];
  sprintf( temp_c_array, "GUI FPS: %.1f", ofGetFrameRate() );
  ofSetColor( 255, 255, 255 );
  font.drawString( temp_c_array, ofGetWidth() - 9 * font_line_height, ofGetHeight() - font_line_height );

  sprintf( temp_c_array, "Out0 FPS: %.1f", output_app0->output_framerate );
  ofSetColor( 255, 255, 255 );
  font.drawString( temp_c_array, ofGetWidth() - 9 * font_line_height, ofGetHeight() );
}



//--------------------------------------------------------------
void GUIApp::shiftHoloImage( unsigned int slice_i, char dir )
{
  // computer & holographic video monitor output coordinate system:
  //
  // 0,0 ---------------------> [COMP_ROWS_PER_HOLOLINE] +X
  //   |
  //   |
  //   |
  //   |
  //   |              *********
  //   V ********************** *potentially invalid region due to Vertical-blanked rows
  //
  //  [N_HOLOLINES]
  //
  //  +Y
  //
  // in this system, increments of Y shift the image down
  //                 decrements of Y shift the image up


  // TODO: study output, verify it begins drawing in the top-left corner like with normal monitors


  // get y-offset, convert into easier-to-understand 2D coordinates
  int temp_x = slices[ slice_i ].y_offset % COMP_ROWS_PER_HOLOLINE;
  int temp_y = slices[ slice_i ].y_offset / COMP_ROWS_PER_HOLOLINE;

  switch( dir )
  {
  case 'l':
    // move the holo-image LEFT one computer row
    // (move all image pixels up one row)
    temp_x--;

    if( temp_x < 0 )
      temp_x = COMP_ROWS_PER_HOLOLINE - 1;

    // check for invalid region due to vertically-blanked rows
    // if in the invalid region, move the image up (decrement Y) until out of it to preserve X
    while( temp_x + temp_y * COMP_ROWS_PER_HOLOLINE >= PIXELS_V_ACTIVE )
      temp_y--;

    break;

  case 'r':
    // move the holo-image RIGHT one computer row
    // (move all image pixels down one row)
    temp_x++;

    if( temp_x >= COMP_ROWS_PER_HOLOLINE )
      temp_x = 0;

    while( temp_x + temp_y * COMP_ROWS_PER_HOLOLINE >= PIXELS_V_ACTIVE )
      temp_y--;

    break;

  case 'u':
    // move the holo-image UP one line
    // (move all image pixels up COMP_ROWS_PER_HOLOLINE lines)
    temp_y--;

    if( temp_y < 0 )
      temp_y = N_HOLOLINES - 1;

    while( temp_x + temp_y * COMP_ROWS_PER_HOLOLINE >= PIXELS_V_ACTIVE )
      temp_y--;

    break;

  case 'd':
    // move the holo-image DOWN one line
    // (move all image pixels down COMP_ROWS_PER_HOLOLINE lines)
    temp_y++;

    // roll back to top in case Y goes beyond limits
    if( temp_y >= N_HOLOLINES || temp_x + temp_y * COMP_ROWS_PER_HOLOLINE >= PIXELS_V_ACTIVE )
      temp_y = 0;

    break;

  default:
    // error message

    printf( "UNDEFINED Y-OFFSET SHIFT COMMAND\n" );

    break;
  }


  int temp_y_offset = temp_x + temp_y * COMP_ROWS_PER_HOLOLINE;


  // update text-based Y-Offset widget
  char temp_c_array[ 10 ];
  sprintf( temp_c_array, "%i", temp_y_offset );
  gui0_slice_YOffset->setText( temp_c_array );

  // update graphical 2D Y-Offset widget
  gui0_slice_YOffset2D->setPoint( ofPoint( (float) temp_x, (float) temp_y ) );

  // set y-offset
  slices[ slice_i ].y_offset = temp_y_offset;

  // redraw config data
  drawSliceConfig( slice_i, sources_fbo.getWidth() + font.getLineHeight(), N_HOLOLINES * slice_i );
}



//--------------------------------------------------------------
void GUIApp::setOutputWindowFullscreen( shared_ptr<ofAppBaseWindow> window, bool fullScreen )
{
  if( fullScreen )
  {
    window->setWindowPosition( -1, 800 );
    window->setFullscreen( true );
  }
  else
  {
    window->setFullscreen( false );
    window->setWindowPosition( 10, gui_window->getWindowPosition().y + gui_window->getHeight() + 40 );
  }


  // restore focus to GUI window by doing a "no net effect" operation on it

  //gui_window->makeCurrent(); // didn't refocus on the GUI window
  //gui_window->update(); // didn't refocus on the GUI window
  //gui_window->showCursor(); // didn't refocus on the GUI window
  // this didn't work either:
  //gui_window->setWindowPosition( (int) gui_window_pos.x, (int) gui_window_pos.y + (output_window0_isFullScreen?1:-1) );

  // THIS IS OMEGA-UBER HACKISH, but it gets the job done for some reason:
  gui_window->toggleFullscreen();
  gui_window->toggleFullscreen();
}



//--------------------------------------------------------------
void GUIApp::keyPressed( int key )
{
  // if we're in the middle of typing in text, don't react in any special way
  if( gui0_general_Title->getFocused() ||
      gui0_general_SliceCount->getFocused() ||
      gui0_general_OutputImageWidth->getFocused() ||
      gui0_slice_Source->getFocused() ||
      gui0_slice_YOffset->getFocused() )
    return;


  switch( key )
  {
  case OF_KEY_ALT: // key modifiers: https://github.com/openframeworks/openFrameworks/issues/2315
    alt_modifier = true;
    // printf( "alt pressed\n" );
    break;
  case OF_KEY_SHIFT:
    shift_modifier = true;
    // printf( "shift pressed\n" );
    break;
  case OF_KEY_CONTROL:
    ctrl_modifier = true;
    // printf( "ctrl ptressed\n" );
    break;



    // translate output image up/down/left/right base with dedicated gamer ASDW 'arrow' keys
  case 'a':
  case 's':
  case 'd':
  case 'w':

    switch( key )
    {
    case 'a':
      shiftHoloImage( focused_slice, 'l' );
      break;
    case 's':
      shiftHoloImage( focused_slice, 'd' );
      break;
    case 'd':
      shiftHoloImage( focused_slice, 'r' );
      break;
    case 'w':
      shiftHoloImage( focused_slice, 'u' );
      break;
    }

    // Y-Offset widgtet updates and config drawing occur in ShiftHoloImage(...)

    break;



  case 'f':
  {
    ofPoint gui_window_pos = gui_window->getWindowPosition();

    output_window0_isFullScreen = !output_window0_isFullScreen;

    setOutputWindowFullscreen( output_window0, output_window0_isFullScreen );
  }
  break;



  case 'e':
    // toggle slice enable

    slices[ focused_slice ].enabled = !slices[ focused_slice ].enabled;

    gui0_slice_Enabled->setChecked( slices[ focused_slice ].enabled );

    drawSliceConfig( focused_slice, sources_fbo.getWidth() + font.getLineHeight(), N_HOLOLINES * focused_slice );

    break;

  case 'b':
    // toggle slice blanking

    slices[ focused_slice ].blanking = !slices[ focused_slice ].blanking;

    gui0_slice_Blank->setChecked( slices[ focused_slice ].blanking );

    drawSliceConfig( focused_slice, sources_fbo.getWidth() + font.getLineHeight(), N_HOLOLINES * focused_slice );

    break;



  case OF_KEY_UP:
  case OF_KEY_DOWN:
  case OF_KEY_LEFT:
  case OF_KEY_RIGHT:
  {

    enum WidgetType { WT_UNKNOWN, WT_Y_OFFSET, WT_SLIDER } last_widget_type = WT_UNKNOWN;
    // consider using ofxDatGuiComponent::getType() and ofxDatGuiType::... instead


    // test for Y_OFFSET-type widgets
    if( last_widget == gui0_slice_YOffset2D )
    {
      last_widget_type = WT_Y_OFFSET;
    }
    else if( last_widget == gui0_general_OutputGain ||
             last_widget == gui0_slice_Angle ||
             last_widget == gui0_slice_RedFreq ||
             last_widget == gui0_slice_RedAmpl ||
             last_widget == gui0_slice_GreenFreq ||
             last_widget == gui0_slice_GreenAmpl ||
             last_widget == gui0_slice_BlueFreq ||
             last_widget == gui0_slice_BlueAmpl )
    {
      last_widget_type = WT_SLIDER;
    }



    switch( last_widget_type )
    {
    case WT_UNKNOWN:
    default:

      break;

    case WT_Y_OFFSET:

      // translate outpupt image up/down/left/right base with dedicated gamer ASDW 'arrow' keys
      switch( key )
      {
      case OF_KEY_UP:
        shiftHoloImage( focused_slice, 'u' );
        break;
      case OF_KEY_DOWN:
        shiftHoloImage( focused_slice, 'd' );
        break;
      case OF_KEY_LEFT:
        shiftHoloImage( focused_slice, 'l' );
        break;
      case OF_KEY_RIGHT:
        shiftHoloImage( focused_slice, 'r' );
        break;
      }

      // Y-Offset widgtet updates and config drawing occur in ShiftHoloImage(...)

      break;

    case WT_SLIDER:
    {
      ofxDatGuiSlider* last_slider = (ofxDatGuiSlider*) last_widget;

      //double value = last_slider->getValue();
      // I think this literally only stores two digits of precision beyond the decimal point,
      // so get the actual value from the slices array

      double value;

      if( last_widget == gui0_slice_Angle )
        value = slices[ focused_slice ].angle;
      if( last_widget == gui0_slice_RedFreq )
        value = slices[ focused_slice ].freq_r;
      if( last_widget == gui0_slice_RedAmpl )
        value = slices[ focused_slice ].ampl_r;
      if( last_widget == gui0_slice_GreenFreq )
        value = slices[ focused_slice ].freq_g;
      if( last_widget == gui0_slice_GreenAmpl )
        value = slices[ focused_slice ].ampl_g;
      if( last_widget == gui0_slice_BlueFreq )
        value = slices[ focused_slice ].freq_b;
      if( last_widget == gui0_slice_BlueAmpl )
        value = slices[ focused_slice ].ampl_b;


      double max = last_slider->getMax(); // modified ofxDatGuiSlider.h to include this
      double min = last_slider->getMin(); // modified ofxDatGuiSlider.h to include this

      double delta = 0.01*( max - min );
      if( ctrl_modifier )// each ALT, SHIFT, CTRL modifier decreases the rate by 1/10
        delta *= 0.1;  // the more modifiers, the smaller the adjustmet
      if( shift_modifier )
        delta *= 0.1;
      if( alt_modifier )
        delta *= 0.1;

      switch( key )
      {
      case OF_KEY_LEFT:
        value -= delta;
        break;
      case OF_KEY_RIGHT:
        value += delta;
        break;
      }

      if( value > max )
        value = max;
      if( value < min )
        value = min;

      last_slider->setValue( value );

      printf( "%s: %.2f\n", last_slider->getLabel().c_str(), value );


      if( last_widget == gui0_slice_Angle )
        slices[ focused_slice ].angle = value;
      if( last_widget == gui0_slice_RedFreq )
        slices[ focused_slice ].freq_r = value;
      if( last_widget == gui0_slice_RedAmpl )
        slices[ focused_slice ].ampl_r = value;
      if( last_widget == gui0_slice_GreenFreq )
        slices[ focused_slice ].freq_g = value;
      if( last_widget == gui0_slice_GreenAmpl )
        slices[ focused_slice ].ampl_g = value;
      if( last_widget == gui0_slice_BlueFreq )
        slices[ focused_slice ].freq_b = value;
      if( last_widget == gui0_slice_BlueAmpl )
        slices[ focused_slice ].ampl_b = value;


    } // case WT_SLIDER:
    break;
    } // switch( last_widget_type )

  } // case OF_KEY_UP / OF_KEY_DOWN / OF_KEY_LEFT / OF_KEY_RIGHT
  break;
  } // switch( key )
}

//--------------------------------------------------------------
void GUIApp::keyReleased( int key )
{
  switch( key )
  {
  case OF_KEY_ALT:
    alt_modifier = false;
    // printf( "alt released\n" );
    break;

  case OF_KEY_SHIFT:
    shift_modifier = false;
    // printf( "shift released\n" );
    break;

  case OF_KEY_CONTROL:
    ctrl_modifier = false;
    // printf( "ctrl released\n" );
    break;
  }
}



//--------------------------------------------------------------
void GUIApp::windowResized( int w, int h )
{
  printf( "GUI window: %ix%i\n", w, h );

  redraw_slices = 4; // output isn't drawn first three times for some reason
}



//--------------------------------------------------------------
void GUIApp::mousePressed( int x, int y, int button )
{
  if( button == OF_MOUSE_BUTTON_LEFT )
    if( x < gui0->getPosition().x && y < n_slices * N_HOLOLINES )
    {
      focused_slice_last = focused_slice;

      focused_slice = y / N_HOLOLINES;

      updateWidgetsFromSlice( focused_slice );

      drawSliceConfig( focused_slice_last, sources_fbo.getWidth() + font.getLineHeight(), N_HOLOLINES * focused_slice_last );

      drawSliceConfig( focused_slice, sources_fbo.getWidth() + font.getLineHeight(), N_HOLOLINES * focused_slice );
    }
}



//--------------------------------------------------------------
void GUIApp::mouseDragged( int x, int y, int button )
{
  if( button == OF_MOUSE_BUTTON_RIGHT )
  {
    debug_x = x;
    debug_y = y;
    printf( "%i, %i\n", x, y );
  }
}



////--------------------------------------------------------------
//void GUIApp::mouseMoved( int x, int y )
//{
//
//}
//
////--------------------------------------------------------------
//void GUIApp::mouseReleased( int x, int y, int button )
//{
//
//}
//
////--------------------------------------------------------------
//void GUIApp::mouseEntered( int x, int y )
//{
//
//}
//
////--------------------------------------------------------------
//void GUIApp::mouseExited( int x, int y )
//{
//
//}
//
//
////--------------------------------------------------------------
//void GUIApp::gotMessage( ofMessage msg )
//{
//
//}
//
////--------------------------------------------------------------
//void GUIApp::dragEvent( ofDragInfo dragInfo )
//{
//
//}

void GUIApp::exit()
{
  printf( "GUIApp: BYE.\n" );

  std::exit( 0 );
}