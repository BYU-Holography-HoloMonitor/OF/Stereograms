#include "GUIApp.h"



//--------------------------------------------------------------
void GUIApp::onButtonEvent( ofxDatGuiButtonEvent e )
{
  printf( "%s pressed\n", e.target->getLabel().c_str() );

  if( e.target == gui0_general_Load )
  {
    ofFileDialogResult openFileResult = ofSystemLoadDialog( "Select a STO file" );

    if( openFileResult.bSuccess )
    {
      //printf( "File selected\n" );

      loadStereogramFromFile( openFileResult.getPath() );
    }
    else
    {
      printf( "Open File canceled\n" );
    }
  }
  else if( e.target == gui0_general_Save )
  {
    ofFileDialogResult saveFileResult = ofSystemSaveDialog( "stereogram_" + ofGetTimestampString() + ".sto", "Save Stereogram File" );
    if( saveFileResult.bSuccess )
    {
      saveStereogramToFile( saveFileResult.getPath() );
    }
    else
    {
      printf( "Save File canceled\n" );
    }
  }
}



//--------------------------------------------------------------
void GUIApp::onDropDownEvent( ofxDatGuiDropdownEvent e )
{
  char temp_char_array[128];

  if( e.target == gui0_general_StereogramMode )
  {

    // TODO: SAVE EXISTING STATE, SLICE SETTINGS?


    StereogramMode backup = stereogram_mode;

    stereogram_mode = (StereogramMode) e.child;

    string mode_name;

    switch( stereogram_mode )
    {
    case GUIApp::POLY_STATIC:

      // initialize slices from demo

      loadStereogramFromFile( "data\\multi static demo\\multi static demo.sto" );

      break;

    case GUIApp::MONO_VIDEO:

      // initialize slices from demo

      loadStereogramFromFile( "data\\video file demo\\video file demo.sto" ); // TODO: flesh this out

      break;

    case GUIApp::QUAD_VIEW:

      // initialize slices from demo

      loadStereogramFromFile( "data\\quad view demo.sto" ); // TODO: flesh this out

      break;

    case GUIApp::MONO_SCREEN:

      // initialize slices from demo

      loadStereogramFromFile( "data\\screen capture demo.sto" ); // TODO: flesh this out

      break;

    default:

      stereogram_mode = backup;

      e.target->setIndex( stereogram_mode );

      break;
    }

    printf( "Stereogram Mode: %s\n", mode_options[stereogram_mode].c_str() );

  }
  else if( e.target == gui0_slice_SelectedSlice )
  {
    // change slice focus and update widgets

    focused_slice_last = focused_slice;

    focused_slice = e.child;

    updateWidgetsFromSlice( focused_slice );
  }

  // the drawing from the drop-down menus pushing everying down needs to be cleared, so redraw everything
  redraw_slices = true;
}



//--------------------------------------------------------------
void GUIApp::onTextInputEvent( ofxDatGuiTextInputEvent e )
{
  // record which widget was used last for keyboard control
  last_widget = e.target;

  printf( "%s: ", e.target->getLabel().c_str() );

  int val = atoi( e.text.c_str() );

  char temp_char_array[128];



  if( e.target == gui0_general_SliceCount )
  {
    // change the number of slices

    if( val < 1 )
    {
      val = 1;
    }
    if( val > N_MAX_SLICES )
    {
      val = N_MAX_SLICES;
    }


    n_slices = val;

    printf( "number of slices changed to %i\n", n_slices );



    // disable + clear all unused slices
    for( int i = n_slices; i < N_MAX_SLICES; i++ )
      clearSlice( i );



    // update widgets; TODO: eliminate code redundancy with loadStereogramFromFile(...)
    if( focused_slice >= n_slices )
    {
      focused_slice = n_slices - 1;
    }
    if( focused_slice_last >= n_slices )
    {
      focused_slice_last = n_slices - 1;
    }

    // update slice count indicator
    sprintf( temp_char_array, "%i", n_slices );
    gui0_general_SliceCount->setText( temp_char_array );

    // update slice selection drop-down menu
    vector<string> selected_slice_options;
    for( int i=0; i < n_slices; i++ )
    {
      sprintf( temp_char_array, "Slice %u", i );
      selected_slice_options.push_back( temp_char_array );
    }
    gui0_slice_SelectedSlice->setNewOptions( selected_slice_options );


    updateWidgetsFromSlice( focused_slice );


    // clear/redraw all slices + slice config printouts

    loadSlicesFromFiles(); // this is an easy way of changing the FBO size and redrawing everything

  }
  else if( e.target == gui0_general_ReloadPeriod )
  {
    // change the reload period

    printf( "%s\n", gui0_general_ReloadPeriod->getText().c_str() );

    float temp_period = stof( gui0_general_ReloadPeriod->getText() );

    // data validation
    if( temp_period < 0.0 )
    {
      temp_period = -temp_period;
    }

    // limit to integer seconds to prevent rapid reloading
    temp_period = round( temp_period );

    //reload_period = temp_period;

    sprintf( temp_char_array, "%.0f", temp_period );
    gui0_general_ReloadPeriod->setText( temp_char_array );

    printf( "Reload Period: %.0f seconds\n", temp_period );

  }
  else if( e.target == gui0_general_OutputImageWidth )
  {
    // change the output width

    if( val < sources_fbo.getWidth() )
    {
      val = sources_fbo.getWidth();

      sprintf( temp_char_array, "%i", val );
      gui0_general_OutputImageWidth->setText( temp_char_array );
    }

    printf( "output image width changed to %i pixels\n", val );

  }
  else if( e.target == gui0_slice_Source )
  {
    // change the source image of a slice & try to load from file

    slices[focused_slice].source = gui0_slice_Source->getText();

    printf( "changed source of slice %i to '%s'\n", focused_slice, slices[focused_slice].source.c_str() );

    loadSlicesFromFiles(); // also redraws slice config printout

  }
  else if( e.target == gui0_slice_YOffset )
  {
    // change the Y-Offset

    while( val < 0 )
    {
      val = 0;
    }
    while( val >= PIXELS_V_ACTIVE )
    {
      val = PIXELS_V_ACTIVE - 1;
    }

    slices[focused_slice].y_offset = val;

    sprintf( temp_char_array, "%i", val );
    gui0_slice_YOffset->setText( temp_char_array );

    int x = val % COMP_ROWS_PER_HOLOLINE;
    int y = val / COMP_ROWS_PER_HOLOLINE;

    gui0_slice_YOffset2D->setPoint( ofPoint( (float) x, (float) y ) );
    // this method had to be modified to be:
    //   void setPoint(ofPoint pt)
    //   {
    //     mWorld = pt;
    //     mPercentX = ( mWorld.x - mBounds.x ) / mBounds.width;  // added this line
    //     mPercentY = ( mWorld.y - mBounds.y ) / mBounds.height; // added this line
    //   }
    // there's still a small bug with this function where the crosshairs + dot will
    // be drawn slightly differently between being set by this method and mouse-drag input

    printf( "%i (%i, %i)\n", val, x, y );

    drawSliceConfig( focused_slice, sources_fbo.getWidth() + font.getLineHeight(), N_HOLOLINES * focused_slice );
  }
}



//--------------------------------------------------------------
void GUIApp::onSliderEvent( ofxDatGuiSliderEvent e )
{
  // record which widget was used last for keyboard control
  last_widget = e.target;

  printf( "%s: %.2f\n", e.target->getLabel().c_str(), e.value );

  StereogramSlice* slice_ptr = &slices[focused_slice];

  if( e.target == gui0_general_OutputGain )
  {
    // output_gain = e.value;
    // don't do anything; when you want the output gain, just read the value of the slider
    // since it's being read often, glitches that occur from read/write races won't have any lasting impact
  }
  else if( e.target == gui0_slice_Angle ||
           e.target == gui0_slice_RedFreq ||
           e.target == gui0_slice_RedAmpl ||
           e.target == gui0_slice_GreenFreq ||
           e.target == gui0_slice_GreenAmpl ||
           e.target == gui0_slice_BlueFreq ||
           e.target == gui0_slice_BlueAmpl )
  {
    if( e.target == gui0_slice_Angle )
    {
      slice_ptr->angle = e.value;

      // TODO: change RGB freq/ampl values, sliders based upon angle
    }
    else if( e.target == gui0_slice_RedFreq )
      slice_ptr->freq_r = e.value;
    else if( e.target == gui0_slice_RedAmpl )
      slice_ptr->ampl_r = e.value;
    else if( e.target == gui0_slice_GreenFreq )
      slice_ptr->freq_g = e.value;
    else if( e.target == gui0_slice_GreenAmpl )
      slice_ptr->ampl_g = e.value;
    else if( e.target == gui0_slice_BlueFreq )
      slice_ptr->freq_b = e.value;
    else if( e.target == gui0_slice_BlueAmpl )
      slice_ptr->ampl_b = e.value;

    drawSliceConfig( focused_slice, sources_fbo.getWidth() + font.getLineHeight(), N_HOLOLINES * focused_slice );
  }
}



//--------------------------------------------------------------
void GUIApp::onToggleEvent( ofxDatGuiToggleEvent e )
{
  printf( "\"%s\": %s\n", e.target->getLabel().c_str(), e.target->getChecked() ? "TRUE" : "FALSE" );

  if( e.target == gui0_general_FlipHorizontal )
  {
    // no action
  }
  else if( e.target == gui0_general_FlipVertical )
  {
    // no action
  }
  else if( e.target == gui0_general_RollVertical )
  {
    // no action
  }
  else if( e.target == gui0_slice_Enabled ||
           e.target == gui0_slice_Blank )
  {
    if( e.target == gui0_slice_Enabled )
      slices[focused_slice].enabled = e.checked; // bool to int conversion
    else if( e.target == gui0_slice_Blank )
      slices[focused_slice].blanking = e.checked; // bool to int conversion

    drawSliceConfig( focused_slice, sources_fbo.getWidth() + font.getLineHeight(), N_HOLOLINES * focused_slice );
  }


}



//--------------------------------------------------------------
void GUIApp::on2DPadEvent( ofxDatGui2dPadEvent e )
{
  // record which widget was used last for keyboard control
  last_widget = e.target;

  if( e.target == gui0_slice_YOffset2D )
  {
    int i = int( e.x ) + int( e.y ) * COMP_ROWS_PER_HOLOLINE;

    if( i < PIXELS_V_ACTIVE )
    {
      slices[focused_slice].y_offset = i;

      char temp[10];
      sprintf( temp, "%i", i );
      gui0_slice_YOffset->setText( temp );

      printf( "%s: %i (%i, %i)\n", e.target->getLabel().c_str(), i, (int) e.x, (int) e.y );

      drawSliceConfig( focused_slice, sources_fbo.getWidth() + font.getLineHeight(), N_HOLOLINES * focused_slice );
    }
  }
}