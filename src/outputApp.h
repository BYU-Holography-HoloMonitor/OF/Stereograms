#pragma once

#include "ofMain.h"
#include "GUIApp.h"

class outputApp : public ofBaseApp
{

public:

	shared_ptr<GUIApp> gui_app;

	ofShader shader;

	unsigned int output_window_index;

	float output_framerate;



	int enabled[N_MAX_SLICES]; // is there a better way we can transmit booleans to the GPU?
	int blanking[N_MAX_SLICES]; // is there a better way we can transmit booleans to the GPU?
	int y_offset[N_MAX_SLICES];
	float freq_k_r[N_MAX_SLICES];
	float freq_k_g[N_MAX_SLICES];
	float freq_k_b[N_MAX_SLICES];
	float ampl_r[N_MAX_SLICES];
	float ampl_g[N_MAX_SLICES];
	float ampl_b[N_MAX_SLICES];



	void setup();
	void update();
	void draw();

	void keyPressed( int key );
	//void keyReleased( int key );
	//void mouseMoved( int x, int y );
	//void mouseDragged( int x, int y, int button );
	//void mousePressed( int x, int y, int button );
	//void mouseReleased( int x, int y, int button );
	//void mouseEntered( int x, int y );
	//void mouseExited( int x, int y );
	void windowResized( int w, int h );
	//void dragEvent( ofDragInfo dragInfo );
	//void gotMessage( ofMessage msg );
  void exit();
};
