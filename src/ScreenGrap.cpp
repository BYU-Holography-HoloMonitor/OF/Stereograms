#include "ScreenGrab.h"

// local helper function to convert std::string to LPCWSTR
// credit to https://stackoverflow.com/questions/27220/how-to-convert-stdstring-to-lpcwstr-in-c-unicode
std::wstring s2ws( const std::string& s )
{
  int len;
  int slength = (int) s.length() + 1;
  len = MultiByteToWideChar( CP_ACP, 0, s.c_str(), slength, 0, 0 );
  wchar_t* buf = new wchar_t[ len ];
  MultiByteToWideChar( CP_ACP, 0, s.c_str(), slength, buf, len );
  std::wstring r( buf );
  delete[] buf;
  return r;
}



//--------------------------------------------------------------
void ScreenGrab::initScreenGrab( string window_title,
                                 int capt_src_x, int capt_src_y,
                                 int capt_src_w, int capt_src_h,
                                 int capt_dst_w, int capt_dst_h )
{
  this->window_title = window_title;
  this->capt_src_x = capt_src_x;
  this->capt_src_y = capt_src_y;
  this->capt_src_w = capt_src_w;
  this->capt_src_h = capt_src_h;
  this->capt_dst_w = capt_dst_w;
  this->capt_dst_h = capt_dst_h;

  // NOTE: the source offset x & y does not include the title bar and menu bar of some programs

  printf( "initScreenGrab: CAUTION: some resolutions (possibly odd numbers) cause program crashes\n" );

  // where to specify offsets?

  //capture_src_width = 320;
  //capture_src_height = 240;

  // Windows 7 window content offsets: [x=7,y=29]

  //capture_dst_width = 140; // 320; // capture_src_width >> 1;// 
  //capture_dst_height = 52 * 2; // 240; //  capture_src_height >> 1;// 

                               // tried scaling image down to 52*2 pixels tall using same aspect ratio:
                               //capture_dst_width = ( capture_src_width * 52 * 2 ) / capture_src_height; // = 138
                               //capture_dst_height = 52 * 2; // = 104
                               // in catpureImg.setFromPixels(...), OF breaks for some reason; I'm betting it's because 138 contains a 23 prime number factor
                               // I changed the capture_dst_width to 140 and it works with the height being 52 * 2



  std::wstring stemp = s2ws( window_title );
  window_handle = FindWindow( NULL, stemp.c_str() );
  if( !window_handle )
    printf( "Window '%s' not found. Defaulting to desktop.\n", window_title.c_str() );

  // when repeating this setup function, do we need to call delete or a destructor for the following three objects?
  hSourceDC = GetDC( window_handle );//GetDC( NULL ); for entire screen
  hCaptureDC = CreateCompatibleDC( hSourceDC );
  hCaptureBitmap = CreateCompatibleBitmap( hSourceDC, capt_dst_w, capt_dst_h );

  bmi.bmiHeader.biSize = sizeof( bmi.bmiHeader );
  bmi.bmiHeader.biWidth = capt_dst_w;
  bmi.bmiHeader.biHeight = capt_dst_h;
  bmi.bmiHeader.biPlanes = 1;
  bmi.bmiHeader.biBitCount = 24;
  bmi.bmiHeader.biCompression = BI_RGB;
  
  // (re)allocate capturedPixelData
  if( capturedPixelData != 0 && capturedPixelData != (unsigned char *) 0xcdcdcdcd )
    delete[] capturedPixelData;
  capturedPixelData = new unsigned char[ capt_dst_w * capt_dst_h * 3 ];

  // (re)allocate captureImg
  if( capture_img.isAllocated() )
    capture_img.~ofImage_();
  capture_img.allocate( capt_dst_w, capt_dst_h, OF_IMAGE_COLOR );
}



//--------------------------------------------------------------
void ScreenGrab::updateScreenGrab()
{
  //hSourceDC = GetDC( window_handle );
  //hCaptureDC = CreateCompatibleDC( hSourceDC );
  //hCaptureBitmap = CreateCompatibleBitmap( hSourceDC, capture_dst_width, capture_dst_height );


  SelectObject( hCaptureDC, hCaptureBitmap );


  // flip right-side up:
  StretchBlt( hCaptureDC, 
              0, capt_dst_h, capt_dst_w, -capt_dst_h, // Dest x y w h
              hSourceDC,
              capt_src_x, capt_src_y, capt_src_w, capt_src_h, // Src x y w h
              SRCCOPY );
  // keep upside-down:
  ///StretchBlt( hCaptureDC, 0, 0, capture_dst_width, capture_dst_height, hSourceDC, 0, 0, capture_src_width, capture_src_height, SRCCOPY );


  //ReleaseDC( window_handle, hSourceDC );

  if( GetDIBits( hCaptureDC, hCaptureBitmap, 0, capt_dst_h, (void*) capturedPixelData, &bmi, DIB_RGB_COLORS ) == 0 )
  {
    cout << "Error: Failed to copy the pixels" << flush;
  }


  //DeleteDC( hCaptureDC );
  //DeleteObject( hCaptureBitmap );


  capture_img.setFromPixels( capturedPixelData, capt_dst_w, capt_dst_h, OF_IMAGE_COLOR, false );
  capture_img.update();
}



//--------------------------------------------------------------
bool ScreenGrab::isReady()
{
  return capture_img.isAllocated() &&
    hSourceDC != 0 && hSourceDC != (HDC) 0xcdcdcdcd &&
    capturedPixelData != 0 && capturedPixelData != (unsigned char *) 0xcdcdcdcd;
}

int ScreenGrab::getCaptSrcX()
{
  return capt_src_x;
}

int ScreenGrab::getCaptSrcY()
{
  return capt_src_y;
}

int ScreenGrab::getCaptSrcW()
{
  return capt_src_w;
}

int ScreenGrab::getCaptSrcH()
{
  return capt_src_h;
}

int ScreenGrab::getCaptDstW()
{
  return capt_dst_w;
}

int ScreenGrab::getCaptDstH()
{
  return capt_dst_h;
}