#version 150
#extension GL_ARB_texture_rectangle : enable
#extension GL_EXT_gpu_shader4 : enable

#define _PI 3.1415926535897932 // 17 digits to fill double float precision (if needed), 7 digits for float precision

#define N_MAX_SLICES 10 // this number must equal that in GUIApp.h


//vec4 - a struct of 4 floats
//dvec4 - a struct of 4 doubles


in layout( pixel_center_integer ) vec4 gl_FragCoord;
// in GLSL, the bottom-left-most pixel of the screen is the origin pixel
// pixels are centered at integer locations with  layout( pixel_center_integer )
//       ie, the bottom-left most pixel with  layout( pixel_center_integer )
//       is [0,0] instead of [0.5,0.5]
// NOTE: texture2DRect( [sampler2DRec], [vec2] ) is on half-pixel boundaries
//       ie, to access pixel [0,0], [vec2] needs to be [0.5,0.5]

out vec4 output_color;
// r,g,b,a are between 0.0-1.0; values are clipped to this range
// a (alpha) is opaque when 1.0, transparent when 0.0
// transparent with things before the shader code started in draw() of ofApp.cpp or equivalent


uniform int PIXELS_H_ACTIVE;        // 3872  as of 22 Jul 2016
uniform int PIXELS_V_ACTIVE;        // 3293  as of 22 Jul 2016
uniform int COMP_ROWS_PER_HOLOLINE; // 64    as of 22 Jul 2016
uniform int N_HOLOLINES;            // 52    as of 22 Jul 2016

//uniform int STEREOGRAM_MODE;
// from GUIApp.h: (18 Jun 2016 DH)
// enum StereogramMode { POLY_STATIC, MONO_VIDEO, QUAD_VIEW, MONO_SCREEN } stereogram_mode;

uniform int n_slices;
uniform float output_gain;
uniform int flip_horizontal;// I want these to be boolean/unsigned ints, but openFrameworks doesn't have the setUniformXX functions to do so
uniform int flip_vertical;// what do the setAttributeXX functions do? http://openframeworks.cc/documentation/gl/ofShader
uniform int roll_vertical;
uniform int output_image_width;


uniform sampler2DRect tex0;
uniform int source_image_width;
// tex0 height is n_slices * N_HOLOLINES

uniform int n_subframes; // 
uniform int subframe_i; // 


uniform int enabled[N_MAX_SLICES];
uniform int blanking[N_MAX_SLICES];

uniform int y_offset[N_MAX_SLICES];

uniform float freq_k_r[N_MAX_SLICES];
uniform float freq_k_g[N_MAX_SLICES];
uniform float freq_k_b[N_MAX_SLICES];

uniform float ampl_r[N_MAX_SLICES];
uniform float ampl_g[N_MAX_SLICES];
uniform float ampl_b[N_MAX_SLICES];

// should we reenable these?
//uniform float gamma;
//uniform float contrast;
//uniform float brightness;


// NOTE: this is the GLSL language, which is similar to but not the same as the C language
// http://www.opengl.org/wiki/Data_Type_%28GLSL%29
// http://www.openframeworks.cc/doxygen/classof_shader.html

void main()
{

	/*
	// debug code to see if the texture is coming through correctly

	int debug_screen_x = int( gl_FragCoord.x );
	int debug_screen_y = int( gl_FragCoord.y );

	if( debug_screen_x < source_image_width && debug_screen_y < n_slices * N_HOLOLINES )
	{
		vec3 debug_rgb = texture2DRect( tex0, vec2( gl_FragCoord.x + 0.5, gl_FragCoord.y + 0.5 ) ).rgb;

		//output_color = vec4( gl_FragCoord.x / source_image_width, gl_FragCoord.y / ( n_slices * N_HOLOLINES) , 0.0, 1.0 );
		output_color.rgb = debug_rgb;
		output_color.a = 1.0;

		return;
	}
	*/



	int ACTIVE_PIXELS_PER_HOLOLINE = int( PIXELS_H_ACTIVE * COMP_ROWS_PER_HOLOLINE );
	float inv_ACTIVE_PIXELS_PER_HOLOLINE = 1.0 / float( ACTIVE_PIXELS_PER_HOLOLINE );



	// general error color: ORANGE
	output_color = vec4( 1.0, 0.5, 0.0, 1.0 );

	vec3 sum_color = vec3( 0.0, 0.0, 0.0 );

	int n_active_images = 0;



	for( int img_i = 0; img_i < n_slices; img_i++ )
	{
		if( enabled[img_i] == 1 )
		{
			n_active_images++;

			vec3 source_rgb;


			if( blanking[img_i] == 1 )
			{
				source_rgb = vec3( 1.0, 1.0, 1.0 );
			}
			else
			{
				int screen_x = int( gl_FragCoord.x );
				int screen_y = int( gl_FragCoord.y );


				// y-offset // TODO: evaluate order of transfomations (y-offset, roll, H-flip, V-flip, etc...)
				screen_y += y_offset[img_i];


				// TODO: the image loops correctly with the following two while(...) blocks, but why???
				while( screen_y < 0 )
					screen_y += COMP_ROWS_PER_HOLOLINE * N_HOLOLINES;
					//screen_y += PIXELS_V_ACTIVE;

				while( screen_y >= PIXELS_V_ACTIVE )
				//while( screen_y >= COMP_ROWS_PER_HOLOLINE * N_HOLOLINES; )
					screen_y -= COMP_ROWS_PER_HOLOLINE * N_HOLOLINES;
					//screen_y -= PIXELS_V_ACTIVE;



				// rolling // TODO: evaluate order of transfomations (y-offset, roll, H-flip, V-flip, etc...)
				if( roll_vertical == 1 )
				{
					// 'rolling' basically takes a number of screen rows and reverses their order one group at a time
					// eg, if the group size is 4
					// and the rows are  0  1  2  3  4  5  6  7  8  9 10 11 12 13 ...
					// they become       3  2  1  0  7  6  5  4 11 10  9  8 15 14 ...
					// in this application, the group size is COMP_ROWS_PER_HOLOLINE

					// start
					// screen_y = 0  1  2  3  4  5  6  7  8  9 10 11 12 13 ...
					int temp1 = int( screen_y / COMP_ROWS_PER_HOLOLINE )* COMP_ROWS_PER_HOLOLINE;
					// temp1    = 0  0  0  0  4  4  4  4  8  8  8  8 12 12 ...
					int temp2 = screen_y - temp1;
					// temp2    = 0  1  2  3  0  1  2  3  0  1  2  3  0  1 ...
					int temp3 = COMP_ROWS_PER_HOLOLINE - 1 - temp2;
					// temp3    = 3  2  1  0  3  2  1  0  3  2  1  0  3  2 ...
					int screen_y = temp1 + temp3;
					// screen_y = 3  2  1  0  7  6  5  4 11 10  9  8 15 14 ...

					// you'll notice that after 'rolling', that we're attempting to access some off-screen rows;
					// I'm not sure what to do in this case

					if( screen_y >= PIXELS_V_ACTIVE )
						screen_y -= PIXELS_V_ACTIVE;

					// or you could just return black if it's outside the screen
				}


				// vertical flip
				if( flip_vertical == 0 ) // TODO: evaluate if this should be == 0
					screen_y = PIXELS_V_ACTIVE - 1 - screen_y;


				// horizontal flip
				if( flip_horizontal == 1 )
					screen_x = PIXELS_H_ACTIVE - 1 - screen_x;


				// check for out-of-bound coordinates
				if( screen_x < 0 || PIXELS_H_ACTIVE <= screen_x || screen_y < 0 || PIXELS_V_ACTIVE <= screen_y )
				{
					if( screen_x < 0 )
						output_color.r = 1.0;
					else if( PIXELS_H_ACTIVE <= screen_x )
						output_color.r = 0.25;

					if( screen_y < 0 )
						output_color.g = 1.0;
					else if( PIXELS_V_ACTIVE <= screen_y )
						output_color.g = 0.25;

					output_color.b = 0.5;

					return;
				}


				// resize
				// calculate source image pixel coordinates based upon screen pixel coordinates
				int screen_i = screen_x + PIXELS_H_ACTIVE * screen_y;
				// grating space is the short and extremely wide image where each row is a hololine
				int grating_x = screen_i % ACTIVE_PIXELS_PER_HOLOLINE;
				int grating_y = int( screen_i * inv_ACTIVE_PIXELS_PER_HOLOLINE );
				// scale
				int source_x = int( ( grating_x * output_image_width ) * inv_ACTIVE_PIXELS_PER_HOLOLINE );
				int source_y = grating_y;
				//int source_x = ( grating_x * ( raw_image_padding + raw_image_width ) ) / ACTIVE_PIXELS_PER_HOLOLINE;
				//int source_y = ( grating_y * raw_image_height ) / N_HOLOLINES;

				/// source_x = int( screen_xy.x ) % raw_image_width;
				/// source_y = raw_image_height - 1 - int( screen_xy.y ) % raw_image_height;



				//if( 0 <= source_x && source_x < raw_image_width && 0 <= source_y && source_y < raw_image_height )
				if( 0 <= source_x && source_x < source_image_width && 0 <= source_y && source_y < N_HOLOLINES )
				{
					// get the color of the current pixel
					// as mentioned at the top, texture coordinates are on half-pixel boundaries
					// ie, to access pixel [0,0], access [0.5,0.5]
					source_rgb = texture2DRect( tex0, vec2( float( source_x ) + 0.5, float( source_y + ( subframe_i + img_i * n_subframes ) * N_HOLOLINES ) + 0.5 ) ).rgb;

					/// debugging:
					///output_color.rgb = source_rgb;
					///output_color.a = 1.0;
					///return;
				}
				else
				{
					//// out of source image bounds: GREEN-CYAN
					//output_color = vec4( 0.0, 1.0, 0.5, 1.0 );
					//return;

					source_rgb = vec3( 0.0, 0.0, 0.0 );
				}
			}


			// calculate amplitude of tone at current x position
			float tone_r = cos( gl_FragCoord.x * freq_k_r[img_i] );
			float tone_g = cos( gl_FragCoord.x * freq_k_g[img_i] );
			float tone_b = cos( gl_FragCoord.x * freq_k_b[img_i] );

			float weight_r = ampl_r[img_i];
			float weight_g = ampl_g[img_i];
			float weight_b = ampl_b[img_i];

			float temp_signal = 0.0;
			temp_signal += tone_r * weight_r * source_rgb.r;
			temp_signal += tone_g * weight_g * source_rgb.g;
			temp_signal += tone_b * weight_b * source_rgb.b;

			float weight_sum = weight_r + weight_g + weight_b;

			if( weight_sum == 0.0 )
				weight_sum = 1.0;

			temp_signal /= weight_sum;

			// TODO: evaluate if we want to select which specific output channel these go to
			sum_color += vec3( temp_signal, temp_signal, temp_signal );
		}
	}


	// average output across images
	if( n_active_images != 0 )
		n_active_images = 1; // ok to change this variable since it won't be used anymore beyond the next 3 lines

	sum_color.r /= n_active_images;
	sum_color.g /= n_active_images;
	sum_color.b /= n_active_images;


	// scale and clip
	sum_color.r = min( 1.0, max( 0.0, sum_color.r * output_gain * 0.5 + 0.5 ) );
	sum_color.g = min( 1.0, max( 0.0, sum_color.g * output_gain * 0.5 + 0.5 ) );
	sum_color.b = min( 1.0, max( 0.0, sum_color.b * output_gain * 0.5 + 0.5 ) );

	output_color.rgb = sum_color;
	output_color.a = 1.0;
}